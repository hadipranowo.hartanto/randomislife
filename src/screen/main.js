import React, { useState, useEffect } from 'react';
import { Http } from '../fetch';
import { MasterHead, NavBar, Animal } from '../component';

const Main = (props) => {
    // CONSTANT
    const title = 'Random is Life';
    const limit = 30;

    // STATE
    const [images, setImages] = useState([]);
    const [quotes, setQuotes] = useState([]);
    const [loadingQuotes, setLoadingQuotes] = useState(true);

    // LYFECYCLE
    useEffect(() => {
        reset();
        getImagesAndQuotes();
    }, []);

    // FUNCTION
    const reset = () => {
        setLoadingQuotes(true);
        setImages([]);
        setQuotes([]);
    };

    /** Public API for images
     * @source https://picsum.photos/
     */
    const getImages = () => {
        let newImages = [];
        for (let i = 0; i < limit; i++) {
            const id = Math.ceil((Math.random() * 1000));
            const imgUrl = `https://picsum.photos/id/${id}/600/400`;
            newImages.push(imgUrl);
        }
        setImages(newImages);
    };

    /** Public API for quotes
     * @source https://pprathameshmore.github.io/QuoteGarden/
     */
    const getQuotes = async () => {
        const qBaseUrl = 'https://quote-garden.herokuapp.com/api/v3/quotes';
        const randomPage = Math.ceil((Math.random() * 100));
        const qPage = 'page=' + randomPage;
        const qLimitPerPage = 'limit=' + limit;
        const qUrl = qBaseUrl + '?' + qPage + '&' + qLimitPerPage;

        setLoadingQuotes(true);
        return await Http.get(qUrl)
            .then((res) => {
                if (res.status === 200 && res.data?.data) {
                    const newQuotes = res.data?.data || [];
                    setQuotes(newQuotes);
                }
            })
            .catch((err) => {
                throw err;
            })
            .finally(() => {
                setLoadingQuotes(false);
            });
    };

    const getImagesAndQuotes = () => {
        getQuotes()
            .then(() => {
                getImages();
            })
            .catch((err) => {
                throw err;
            })
    };

    const onImageError = (idx) => {
        const alt = `https://picsum.photos/600/400`;
        let imgs = [...images];
        imgs[idx] = alt;
        setImages(imgs);
    };

    // RENDER FUNCTION
    const ImageQuoteParsel = () => {
        return quotes.map((val, idx) => {
            const isEven = (idx % 2) === 0 ? true : false;
            const author = val.quoteAuthor;
            const text = val.quoteText;
            const imgUrl = images[idx];

            if (isEven) {
                return (
                    <div class="row gx-0 mb-5 mb-lg-0 justify-content-center">
                        <div class="col-lg-6">
                            <img class="img-fluid" src={imgUrl} onError={() => onImageError(idx)} alt="..." />
                        </div>
                        <div class="col-lg-6">
                            <div class="bg-black text-center h-100 project">
                                <div class="d-flex h-100">
                                    <div class="project-text w-100 my-auto text-center text-lg-left">
                                        <h4 class="text-white">{author}</h4>
                                        <p class="mb-0 text-white-50">{text}</p>
                                        <hr class="d-none d-lg-block mb-0 ms-0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            } else {
                return (
                    <div class="row gx-0 justify-content-center">
                        <div class="col-lg-6">
                            <img class="img-fluid" src={imgUrl} onError={() => onImageError(idx)} alt="..." />
                        </div>
                        <div class="col-lg-6 order-lg-first">
                            <div class="bg-black text-center h-100 project">
                                <div class="d-flex h-100">
                                    <div class="project-text w-100 my-auto text-center text-lg-right">
                                        <h4 class="text-white">{val.quoteAuthor}</h4>
                                        <p class="mb-0 text-white-50">{val.quoteText}</p>
                                        <hr class="d-none d-lg-block mb-0 me-0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        });
    };

    // MAIN RENDER
    return (
        <>
            <NavBar title={title} />

            <MasterHead
                title={title}
                description={"A random image and quote generator to lighten up your mood!"}
            />

            <Animal />

            {/* Main Body */}
            <section class="projects-section bg-light" id="projects">
                <div class="container px-4 px-lg-5">
                    <div class="text-center">
                        <h2 class="text-black">Randomness is Amazing!</h2>
                        <h4 class="text-black">Feel refreshed and new with random quote!</h4>
                    </div>
                    {
                        loadingQuotes
                            ? (
                                <div class="text-center">
                                    <div class="spinner-border" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                            )
                            : (
                                <ImageQuoteParsel />
                            )
                    }

                </div>
            </section>
        </>
    );
};

export default Main;