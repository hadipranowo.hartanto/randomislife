import React from 'react';
import PropTypes from 'prop-types';

const MasterHead = (props) => {

};

MasterHead.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string
};

MasterHead.defaultProps = {
    title: '',
    description: ''
};

export default MasterHead;