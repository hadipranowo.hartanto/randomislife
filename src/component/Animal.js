import React, { useState } from 'react';

const Animal = () => {
    const [animalUrl, setAnimalUrl] = useState('');

    /** Public API for animals
     * @source https://cataas.com/
     */
    const getAnimal = () => {
        const url1 = 'https://cataas.com/cat';
        const url2 = 'https://cataas.com/cat/cute';

        let urls = [
            'https://cataas.com/cat/cute',
            'https://cataas.com/cat/cute/says/ServeMeHooman',
            'https://cataas.com/cat/cute/says/iLoveYou',
            'https://cataas.com/cat/cute/says/Hellow',
            'https://cataas.com/cat/cute/says/Miawwwwww',
            'https://cataas.com/cat/cute/says/GiveMeFoodddd'
        ];

        urls.forEach((val, idx) => {
            const newUrl = val.replace('/cute', '');
            urls.push(newUrl);
        });

        const randomIdx = Math.ceil((Math.random() * urls.length)) - 1;
        setAnimalUrl(urls[randomIdx]);
    };

    return (
        <section class="projects-section bg-light" id="animal">
            <div class="container px-4 px-lg-5 d-flex h-100 align-items-center justify-content-center">
                <div class="d-flex justify-content-center">
                    <div class="text-center">
                        <h2>Call your companion to accompany you today!</h2>
                        <div class="d-grid gap-2">
                            <button
                                class="btn btn-dark"
                                onClick={getAnimal}
                            >Summon</button>
                        </div>
                        {animalUrl
                            ? (
                                <img class="img-fluid mb-3 mb-lg-0" src={animalUrl} alt="..." />
                            )
                            : (null)
                        }
                    </div>
                </div>
            </div>
        </section>
    )
};

export default Animal;