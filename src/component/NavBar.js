import React from 'react';
import PropTypes from 'prop-types';

const NavBar = (props) => {
    const { title } = props;

    return (
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="#page-top">{title}</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item"><a class="nav-link" href="#about" onClick={() => {
                            window.location.reload()
                        }}>Reload</a></li>
                        {/* <li class="nav-item"><a class="nav-link" href="#projects">Projects</a></li>
                        <li class="nav-item"><a class="nav-link" href="#signup">Contact</a></li> */}
                    </ul>
                </div>
            </div>
        </nav>
    )
};

NavBar.propTypes = {
    title: PropTypes.string,
};

NavBar.defaultProps = {
    title: '',
};

export default NavBar;